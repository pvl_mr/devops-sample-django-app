<h1>Test assignment for DevOps practicum</h1>


Sample Python application on Django with PostgreSQL database.

<h3>Deployment</h3>

____

- install docker compose
- clone project

* Set variables for database in .env file in project root:
```yaml
    DJANGO_DB_HOST=postgresql
    DJANGO_DB_NAME=app
    DJANGO_DB_USER=worker
    DJANGO_DB_PASS=worker
    DJANGO_DB_PORT="5432"
    DJANGO_DEBUG="False"
```

* start containers:
```shell
docker comopose up
```
